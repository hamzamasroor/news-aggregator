# News Aggregator

### Description

For this assignment you have to implement an application that aggregates news from two different APIs. The APIs you’ll be using are Reddit and [News API](https://newsapi.org/). This application should be running on your localhost and serve the result in JSON format from an endpoint whenever it gets a request. You are allowed to use 3rd party wrappers of these APIs.

### Part I

The two functionalities that need to be implemented are “list” and “search”.

This is an example request for a generic GET request.

    > Request
    GET /news   HTTP/1.1
    Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ
    Accept: application/json
    
    > Response
    [
      {
        "headline": "Human organs can be stored for three times as long in major breakthrough for transplants",  // Headline of the article
        "link": "https://www.telegraph.co.uk/science/2019/09/09/human-organs-can-stored-three-times-long-major-breakthrough/",  // Link of the article
        "source": "reddit" // Source that you retrieved this news from
      },
      {
        "headline": "Depth of Field: The Shared Memory of One World Trade Center",
        "link": "https://www.wired.com/story/one-world-trade-center-history-future/",
        "source": "newsapi"
      },
    ]

You should also implement a feature that allows someone to search.

    > Request
    GET /news?query=bitcoin   HTTP/1.1
    Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ
    Accept: application/json
    
    > Response
    [
      {
        "headline": "IRS goes after cryptocurrency owners for unpaid taxes",
        "link": "https://www.cbsnews.com/news/own-bitcoin-irs-pursues-cryptocurrency-owners-for-unpaid-taxes/",
        "source": "reddit"
      },
      {
        "headline": "Skirting US sanctions, Cubans flock to cryptocurrency to shop online, send funds",
        "link": "https://www.channelnewsasia.com/news/business/skirting-us-sanctions--cubans-flock-to-cryptocurrency-to-shop-online--send-funds-11901148",
        "source": "newsapi"
      },
    ]

*Suggestion:* Try to use /r/news for Reddit and the general category for News API (you don’t have to but it’s worth checking out).

You have to at least return the three fields that are in the sample requests above. You can add more fields if you’d like.

### Part II

Store the data from the API into a database such that, if a request is repeated, the response is returned from the database and does not need a new API call.

Again, you must store at least the three fields from Part I and any other fields you decide to store.

You should also set an expiry on this data. If a user makes a request that you have seen in the past, but it is past the expiry limit, you should make a fresh call to the API

Note that you should not store this data in the cache, but rather in the database.

### Part III

Implement a feature that allows a user to mark/unmark an article as their favourite and also fetch all of their favourite articles, such that the most recently favorited ones show first.

To mark an article as user's favourite, your end point should be able to accept `user` and `id` as parameters.

This is an example request for a POST request to mark an article as favourite


    > Request
    POST /news/favourite?user=mpopatia&id=5  HTTP/1.1
    Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ
    Accept: application/json
    
    > Response
    [
      {
        "user": "mpopatia",
        "favorite": true,
        "id": 21,
        "headline": "Human organs can be stored for three times as long in major breakthrough for transplants",
        "link": "https://www.telegraph.co.uk/science/2019/09/09/human-organs-can-stored-three-times-long-major-breakthrough/",
        "source": "reddit"
      },
    ]


To unmark an article as favourite, calling the same end point for same `user` and `id` should unmark the article as favourite

Example


    > Request
    POST /news/favourite?user=mpopatia&id=5  HTTP/1.1
    Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ
    Accept: application/json
    
    > Response
    [
      {
        "user": "mpopatia",
        "favorite": false,
        "id": 21,
        "headline": "Human organs can be stored for three times as long in major breakthrough for transplants",
        "link": "https://www.telegraph.co.uk/science/2019/09/09/human-organs-can-stored-three-times-long-major-breakthrough/",
        "source": "reddit"
      },
    ]


To fetch user's favourite articles your end point should be able to accept `user` as a parameter.


    > Request
    GET /news/favourite?user=mpopatia   HTTP/1.1
    Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ
    Accept: application/json
    
    > Response
    [
      {
        "id": 42,
        "headline": "IRS goes after cryptocurrency owners for unpaid taxes",
        "link": "https://www.cbsnews.com/news/own-bitcoin-irs-pursues-cryptocurrency-owners-for-unpaid-taxes/",
        "source": "reddit"
      },
      {
        "id": 57,
        "headline": "Skirting US sanctions, Cubans flock to cryptocurrency to shop online, send funds",
        "link": "https://www.channelnewsasia.com/news/business/skirting-us-sanctions--cubans-flock-to-cryptocurrency-to-shop-online--send-funds-11901148",
        "source": "newsapi"
      },
    ]


You may change the response for Part II to include `id` as well for each article, so that a user can mark it as their favourite.

You are also required to return appropriate responses where the parameters are missing or incorrect

### Constraints

You have to use **Django** and a **relational database (SQLite)** for this task.

This needs to be a running Python application on your localhost that serves an HTTP request not a console application.

### Submission

You should upload your code to a Github/Bitbucket repository (private or public) and share it with [musab@stellic.com](mailto:musab@stellic.com) and [shakaib@stellic.com](mailto:shakaib@stellic.com). Your repository should have a README.md that explains how to run the code and if you’ve done anything extra.

The application that you submit must have thoughtful design decisions, well documented and unit tested code.

You are free to ask questions/clarifications at [musab@stellic.com](mailto:musab@stellic.com) and [shakaib@stellic.com](mailto:shakaib@stellic.com). You will ***not*** be penalized for this.

### Assessment

This assignment is meant to test: 

- Proficiency with Python and Django
- Ability to understand and use 3rd party APIs
- Ability to parse different forms of data
- Ability to design an extendible database schema
- Ability to use Version Control
- Ability to write unit tests
- Ability to write documentation
- Ability to return appropriate response codes and error messages

**What we will be looking for:**

- Great code design and architecture that is extendible to more 3rd party news apps or more fields
- Well documented and clean code with unit tests

*Best of luck!*